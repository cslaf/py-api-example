## Requirements

Packages are managed with poetry. To install the required packages, run the following command:

```bash
poetry install
```

## Pipeline scripts

bump.sh - Bumps the version number in the pyproject.toml file and commits the change to the repository.

release.sh - Creates the INCREMENTAL_CHANGELOG.md file and publishes it as a gitlab release.


## Repo setup

```bash
pip install --user -U Commitizen
cz init ## to initialize commitizen with version number
```

## Adding dependencies

```bash
poetry add <package-name>
# if you get failed to unlock the collection error, disable keyring with:
export PYTHON_KEYRING_BACKEND=keyring.backends.fail.Keyring
```

## Running

```bash
poetry run start
```

