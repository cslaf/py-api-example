''' App Entrypoint '''
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from pydantic import BaseModel
import uvicorn

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origin_regex="https?://.*",
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

def start(reload: bool = True):
    """Launched with `poetry run start` at root level"""
    uvicorn.run("py_api.main:app", host="0.0.0.0", port=8000, reload=reload)

if __name__ == "__main__":
    start(reload=False)

class HealthCheckResponse(BaseModel):
    ''' Response model for health check '''
    status: str = "ok"

@app.get("/health",
        include_in_schema=False)
async def health() -> HealthCheckResponse :
    ''' Health check endpoint '''
    return HealthCheckResponse()
