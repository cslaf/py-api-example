#!/bin/bash

exit_code=0
poetry run cz bump --annotated-tag --changelog --yes || exit_code=$?
echo "cz bump exit code $exit_code"
if [ $exit_code -eq 0 ]; then
    echo "releases"
    git tag --list
    git push --follow-tags https://deploy:${DEPLOYMENT_TOKEN}@${CI_SERVER_HOST}/${CI_PROJECT_PATH}.git HEAD:${CI_COMMIT_BRANCH}
elif [ $exit_code -eq 21 ]; then
    echo "Skipping push with no version change"
elif [ $exit_code -eq 3 ]; then
    echo "Skipping push with no commits" else echo "cz error code $exit_code"
    exit $exit_code
fi;
