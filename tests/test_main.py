''' Tests for main module '''
from fastapi.testclient import TestClient
from httpx import Response #, WSGITransport
from py_api.main import app


def test_health_check():
    ''' Test health check endpoint '''
    client = TestClient(app)
    response : Response = client.get("/health")
    assert response.status_code == 200
    assert response.json() == {"status": "ok"}
