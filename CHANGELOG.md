## 0.3.2 (2024-07-13)

### Fix

- fix code coverage display

## 0.3.1 (2024-07-13)

### Fix

- update containerfile from and release script, cleanup pipeline

## 0.3.0 (2024-07-13)

### Feat

- add coverage report to pipeline and clean up stages
- clean up tox.ini file

## 0.2.2 (2024-02-04)

### Fix

- Containerfile and startup script

## 0.2.1 (2024-02-04)

### Fix

- test tagging pipeline

## 0.2.0 (2024-02-04)

### Feat

- log tagging

### Fix

- release.sh script

## 0.1.2 (2024-02-03)

### Fix

- bash script again

## 0.1.1 (2024-02-03)

### Fix

- bump script

## 0.1.0 (2024-02-03)

### Feat

- working pipeline (hopefully)

## 0.0.1 (2024-02-03)

## v0.0.1 (2024-02-03)
